<?php

class Hash
{
	
	/**
	 *
	 * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
	 * @param string $data The data to encode
	 * @param string $key The Key to encrypt and decrypt
	 * @return string The hashed/salted data
	 */
	public static function create($algo, $data, $key)
	{
		
		$context = hash_init($algo, HASH_HMAC, $key);
		hash_update($context, $data);
		
		return hash_final($context);
		
	}
        
	
}